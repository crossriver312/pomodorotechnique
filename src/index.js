import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import App from './app.vue'
import blog from './pages/blog.vue'
import ctof from './pages/ctof.vue'
import chart from './pages/chart.vue'
import simpleGame from './pages/simpleGame.vue'

const router = new VueRouter({
  mode:'history',
  base:__dirname,
  routes:[
    {
      path:'/blog',
      name:'blog',
      component:blog
    },
    {
      path:'/ctof',
      name:'ctof',
      component: ctof
    },
    {
      path:'/chart',
      name:'chart',
      component: chart
    },
    {
      path:'/simpleGame',
      name:'simpleGame',
      component: simpleGame
    },
    { path: '/*'}
  ]
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})