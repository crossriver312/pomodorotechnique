// webpack.config.js
const path = require('path');
const webpack = require('webpack');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const babelrc = require('./babelrc');

const modulePath = path.join(__dirname, '/node_modules/');
const elementUITheme = path.join(modulePath, 'element-ui/lib/theme-chalk/index.css')

module.exports = (env, { mode }) => ({
  output: {
    filename: "main.[hash:8].js"
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        loader: 'vue-loader',
        options: { shadowMode: true }
      },
      {
        test: /\.js$/,
        include: /web-component-wrapper/,
        loader: 'babel-loader',
        options: babelrc
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: babelrc
      },
      {
        enforce: 'pre',
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(sa|sc|c)ss$/,
        include: /customElements/,
        use: [
          {
            loader: 'vue-style-loader',
            options: { shadowMode: true }
          },
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.(sa|sc|c)ss$/,
        exclude: /customElements/,
        use: [
          {
            loader : MiniCssExtractPlugin.loader,
          },
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.(mp3|ogg|fnt|ttf|woff)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[hash:8].[ext]',
          outputPath: 'assets/',
          publicPath: '/assets/'
        },
      },
      {
        test: /\.(gif|png|jpg|jpeg|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[hash:8].[ext]',
          outputPath: 'assets/images',
          publicPath: '/assets/images'
        },
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    new MiniCssExtractPlugin({
      filename: "styles/[name].[hash:8].css",
    }),
    new webpack.LoaderOptionsPlugin({
      options: {
        eslint: {
          emitWarning: true,
        },
      } 
    }),
    new webpack.DefinePlugin({
      DEVELOPMENT: mode === 'development'
    }),
  ],
  resolve: {
    alias: {
      elementUITheme
    },
    extensions: ['.js', '.json']
  },
  devServer: {
    port: 5555,
    proxy: {
      '/tac/api': {
        target: {
          host: 'localhost',
          protocol: 'http:',
          port: 8088,
        },
      },
    },
  },

});
